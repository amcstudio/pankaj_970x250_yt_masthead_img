window.onload = function() {

	if (Enabler.isInitialized()) {
		pageLoadedHandler();
	} else {
		Enabler.addEventListener(
			studio.events.StudioEvent.INIT, pageLoadedHandler);
	}

}

function pageLoadedHandler(){
	if (Enabler.isVisible()) {
		init();
	}
	else {
		Enabler.addEventListener(studio.events.StudioEvent.VISIBLE, init);
	}
}