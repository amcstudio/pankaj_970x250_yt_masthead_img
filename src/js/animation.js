'use strict';
~ function() {
    var $ = TweenMax,
        stripesTopDistance,
        numStripes = 4,
        easing = Power2.easeOut,
        exit = document.getElementById('exit'),
        blueStripes = document.getElementById('blueStripes'),
        stripe = document.getElementsByClassName('stripe');

    window.init = function() {
        
        exit.addEventListener('click', exitClickHandler);
        exit.addEventListener('mouseover', mouseOverHandler);
        exit.addEventListener('mouseleave', mouseOutHandler);

        createStripesAnimation();
        playAnimation();

    }


    function mouseOverHandler(){
        $.to('#cta', 1, { scale:1.25, rotation:0.01, force3D:true, ease: Power4.easeOut   });
    }

    function mouseOutHandler(){
        $.to('#cta', 1, { scale:1, rotation:0.01, force3D:true, ease: Power4.easeOut   });
    }

    function exitClickHandler() {
        $.set(['#logo', '#subLine', '#cta'] , {x:0});
        $.set(stripe, {x: 2770, width:3880});
        $.set(blueStripes, {scale: .27});
        $.set(product, {scale: .43, x: -427, y: -82});
        exit.removeEventListener('mouseover', mouseOverHandler);
        exit.removeEventListener('mouseleave', mouseOutHandler);
        $.set('#cta', { scale:1});
        Enabler.exit('BackgroundExit');
    }

    function createStripesAnimation() {
        for (var i = 0; i < numStripes; i++) {
            var stripe = document.createElement('div');
            stripe.className = 'stripe';
            blueStripes.appendChild(stripe);
            stripesTopDistance = stripe.offsetHeight * 2;
            stripe.style.top = (stripesTopDistance * i) + 'px';
            $.to(stripe, 4.5, { delay: i * .35, x: 2770, width:3880, force3D: true, rotation: 0.01, ease: easing });
        }
        $.to(blueStripes, 5, { scale: .27, ease: easing });
    }   

    function playAnimation() {
        var tl = new TimelineMax();
        tl.to(product, 5, { delay: 0.2, scale: .43, force3D: true, rotation: 0.01, x: -427, y: -82, ease: easing });
        tl.addLabel('frameTwo', '-=3');
        tl.to('#logo', 4, {  x:0,  ease: Power4.easeOut }, 'frameTwo');
        tl.to('#subLine', 4, { delay: .5, x:0, ease: Power4.easeOut  }, 'frameTwo');
        tl.to('#cta', 4, { delay: .75, x:0, ease: Power4.easeOut   }, 'frameTwo');
    }

}();
